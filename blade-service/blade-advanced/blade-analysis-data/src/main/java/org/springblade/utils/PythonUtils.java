package org.springblade.utils;

import org.springblade.core.tool.api.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Python工具类
 * @author 李家民
 */
public class PythonUtils {

	/** Python环境变量 */
	private static final String PYTHON_ENV = "E:\\27_python 3\\python";

	/**
	 * 执行python文件并记录
	 * @param pathFile
	 * @return
	 */
	public static R executeAndRecord(String pathFile) {
		Process proc;
		try {
			proc = Runtime.getRuntime().exec(PYTHON_ENV + " " + pathFile);
			BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			String line = null;
			StringBuilder stringBuilder = new StringBuilder();
			while ((line = in.readLine()) != null) {
				stringBuilder.append(line + "\n");
			}
			in.close();
			proc.waitFor();
			return R.data(stringBuilder);
		} catch (IOException e) {
			e.printStackTrace();
			return R.fail("IOException");
		} catch (InterruptedException e) {
			e.printStackTrace();
			return R.fail("InterruptedException");
		}
	}
}
