package org.springblade;

import org.springblade.core.cloud.client.BladeCloudApplication;
import org.springblade.core.launch.BladeApplication;

/**
 * MQTT客户端
 * @author 李家民
 */
@BladeCloudApplication
public class MqttClientApplication {
	public static void main(String[] args) {
		BladeApplication.run("blade-mqtt-client", MqttClientApplication.class, args);
	}
}
