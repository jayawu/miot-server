package org.springblade.netty.business.handle.system;

import org.springblade.core.tool.api.R;
import org.springblade.netty.business.MsgCode;
import org.springblade.netty.business.dispatcher.MsgProcessor;
import org.springblade.netty.protocol.GameSession;
import org.springblade.netty.protocol.request.ServerRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * 心跳包
 * @author lijiamin
 */
public class HeartbeatHandler extends MsgProcessor {

	/** 一个无用的心跳数据 */
	private static Map<String, Object> mapHeartbeat = new HashMap<>();

	@Override
	public R process(GameSession gameSession, ServerRequest serverRequest) throws Exception {
		return super.typeJudge(gameSession, 200, R.data(mapHeartbeat), MsgCode.CODE_30.getResp(), "心跳返回");
	}
}
