package org.springblade.netty;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.undertow.util.CopyOnWriteMap;
import org.springblade.netty.business.dispatcher.MsgDispatcher;
import org.springblade.netty.business.handle.system.SimpleLimitedTraffic;
import org.springblade.netty.codec.BusinessMsgDecoder;
import org.springblade.netty.protocol.GameSession;
import org.springblade.netty.protocol.request.ServerRequest;
import org.springblade.netty.protocol.response.ResponseMsg;

import java.net.SocketAddress;
import java.util.HashSet;
import java.util.Set;
import java.util.function.BiConsumer;

/**
 * 处理器
 * @author 李家民
 */
public class NettyServerInboundHandler extends ChannelInboundHandlerAdapter {

	/** 通道储存 */
	private static CopyOnWriteMap<String, GameSession> mapChannel = new CopyOnWriteMap<>();

	/** 消息分发处理器 */
	private static MsgDispatcher msgDispatcher = new MsgDispatcher();

	/**
	 * 外部更新通道
	 * @param address     链接地址
	 * @param gameSession 会话
	 */
	public static void updateMapGameSession(String address, GameSession gameSession) {
		mapChannel.put(address, gameSession);
	}

	/**
	 * 关闭通道
	 * @param address
	 */
	public static void closeGameSession(String address) {
		Channel channel = mapChannel.get(address).getChannelHandlerContext().channel();
		channel.close();
	}

	/**
	 * 消息广播推送-是否走MQ还是Feign
	 * @param msg 消息体
	 */
	public static void sendBroadcast(ResponseMsg msg) {
		Set<String> setAddress = new HashSet<>();
		// 广播
		mapChannel.forEach(new BiConsumer<String, GameSession>() {
			@Override
			public void accept(String address, GameSession gameSession) {
				Channel channel = gameSession.getChannelHandlerContext().channel();
				if (channel.isActive()) {
					channel.writeAndFlush(msg);
				} else {
					setAddress.add(address);
				}
			}
		});
		// 将失活连接铲除
		for (String address : setAddress) {
			mapChannel.remove(address);
			BusinessMsgDecoder.delMsgMap(address);
		}
	}

	/**
	 * 通道关闭
	 * @param ctx
	 * @throws Exception
	 */
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		String remoteAddress = ctx.channel().remoteAddress().toString();
		mapChannel.remove(remoteAddress);
		SimpleLimitedTraffic.removeAddressBucket(remoteAddress);
		BusinessMsgDecoder.delMsgMap(remoteAddress);
		System.out.println(remoteAddress + " ------ channel Inactive！！");
		super.channelInactive(ctx);
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		SocketAddress remoteAddress = ctx.channel().remoteAddress();
		System.out.println(remoteAddress.toString() + " ------ channel Active！！");
		mapChannel.put(remoteAddress.toString(), new GameSession(ctx));
		BusinessMsgDecoder.createMsgMap(remoteAddress.toString());
		super.channelActive(ctx);
	}

	/**
	 * 消息读取
	 * @param ctx
	 * @param msg
	 * @throws Exception
	 */
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		ServerRequest serverRequest = (ServerRequest) msg;
		msgDispatcher.dispatchMsg(mapChannel.get(ctx.channel().remoteAddress().toString()), serverRequest);
	}

	/**
	 * 数据读取完成后的操作
	 * 1. writeAndFlush代表写入+刷新
	 * 2. 还需要对返回的数据进行编码
	 * @param ctx
	 * @throws Exception
	 */
	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		super.channelReadComplete(ctx);
	}

	/**
	 * 发生异常后, 一般是需要关闭通道
	 * @param ctx
	 * @param cause
	 * @throws Exception
	 */
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		ctx.close();
		cause.printStackTrace();
		super.exceptionCaught(ctx, cause);
	}
}
