package org.springblade.common.view;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 饼干视图通用
 * @author lijiamin
 */
@Data
@NoArgsConstructor
public class BiscuitsView<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 标题值 */
	private Map<String, Map<String,String>> titles;

	/** 总量 */
	private T total;

	/** 数据集 */
	private List<Map<String, T>> data;

}
