INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1563826238399721473', '1123598815738675213', 'logaudit', '行为审计', 'menu', '/log/logaudit', NULL, 1, 1, 0, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1563826238399721474', '1563826238399721473', 'logaudit_add', '新增', 'add', '/log/logaudit/add', 'plus', 1, 2, 1, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1563826238399721475', '1563826238399721473', 'logaudit_edit', '修改', 'edit', '/log/logaudit/edit', 'form', 2, 2, 2, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1563826238399721476', '1563826238399721473', 'logaudit_delete', '删除', 'delete', '/api/blade-log/logaudit/remove', 'delete', 3, 2, 3, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1563826238399721477', '1563826238399721473', 'logaudit_view', '查看', 'view', '/log/logaudit/view', 'file-text', 4, 2, 2, 1, NULL, 0);
