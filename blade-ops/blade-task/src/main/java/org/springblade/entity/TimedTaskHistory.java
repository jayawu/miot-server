package org.springblade.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 定时任务执行历史
 */
@Data
@NoArgsConstructor
@TableName("blade_timed_task_history")
public class TimedTaskHistory implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String methodName;
	private String startTime;
	private String endTime;
	private String computationTime;
	private Integer status;
	private String description;

	public TimedTaskHistory(String methodName, String startTime, String endTime, String computationTime, Integer status, String description) {
		this.methodName = methodName;
		this.startTime = startTime;
		this.endTime = endTime;
		this.computationTime = computationTime;
		this.status = status;
		this.description = description;
	}
}
