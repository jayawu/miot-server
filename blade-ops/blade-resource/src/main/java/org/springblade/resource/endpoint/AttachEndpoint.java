package org.springblade.resource.endpoint;

import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springblade.core.boot.ctrl.BladeController;
import org.springblade.core.oss.model.BladeFile;
import org.springblade.core.tool.api.R;
import org.springblade.resource.entity.Attach;
import org.springblade.resource.service.IAttachService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;

/**
 * 附件表 控制器
 * @author Blade
 * @since 2022-10-03
 */
@RestController
@AllArgsConstructor
@RequestMapping("/attach/endpoint")
@Api(value = "附件表", tags = "附件表接口")
public class AttachEndpoint extends BladeController {

	private IAttachService attachService;

	/**
	 * 上传文件
	 * @param file 文件
	 * @return ObjectStat
	 */
	@PostMapping("/put-file")
	@CrossOrigin
	public R<Attach> putFile(@RequestParam MultipartFile file) {
		return R.data(attachService.putFile(file));
	}

	/**
	 * 下载文件
	 * @param id
	 * @return
	 * @throws FileNotFoundException
	 */
	@GetMapping("/download/{id}")
	@CrossOrigin
	public ResponseEntity<byte[]> downloadFile(@PathVariable("id") String id) throws FileNotFoundException {
		return attachService.downloadFile(id);
	}

}
