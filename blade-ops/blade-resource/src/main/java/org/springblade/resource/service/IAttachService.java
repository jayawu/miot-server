/**
 * Copyright (c) 2018-2028, Chill Zhuang 庄骞 (smallchill@163.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springblade.resource.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.core.mp.base.BaseService;
import org.springblade.resource.entity.Attach;
import org.springblade.resource.vo.AttachVO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

/**
 * 附件表 服务类
 * @author Blade
 * @since 2022-10-03
 */
public interface IAttachService extends BaseService<Attach> {

	/**
	 * 自定义分页
	 * @param page
	 * @param attach
	 * @return
	 */
	IPage<AttachVO> selectAttachPage(IPage<AttachVO> page, AttachVO attach);

	/**
	 * 上传文件
	 * @param file
	 * @return
	 */
	Attach putFile(MultipartFile file);

	/**
	 * 下载文件
	 * @param id
	 * @return
	 */
	ResponseEntity<byte[]> downloadFile(String id);
}
