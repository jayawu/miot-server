/**
 * Copyright (c) 2018-2028, Chill Zhuang 庄骞 (smallchill@163.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springblade.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

import org.springblade.core.mp.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 实体类
 * @author Blade
 * @since 2022-08-27
 */
@Data
@TableName("blade_equipment")
@ApiModel(value = "Equipment对象", description = "Equipment对象")
public class Equipment implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "主键")
	private Long id;
	/**
	 * 设备序号
	 */
	@ApiModelProperty(value = "设备序号")
	private String dSerial;
	/**
	 * 通道序号
	 */
	@ApiModelProperty(value = "通道序号")
	private String cSerial;
	/**
	 * 位置id
	 */
	@ApiModelProperty(value = "位置id")
	private Long locationId;
	/**
	 * 设备父类型
	 */
	@ApiModelProperty(value = "设备父类型-关联字典")
	private Integer typeParent;
	/**
	 * 设备子类型
	 */
	@ApiModelProperty(value = "设备子类型-关联字典")
	private Integer typeSub;
	/**
	 * 名称
	 */
	@ApiModelProperty(value = "名称")
	private String name;
	/**
	 * 当前数据值
	 */
	@ApiModelProperty(value = "当前数据值")
	private String value;
	/**
	 * 备注
	 */
	@ApiModelProperty(value = "备注")
	private String remark;
	/**
	 * 设备在线状态
	 */
	@ApiModelProperty(value = "设备在线状态")
	private Integer online;
	/**
	 * 设备IP地址
	 */
	@ApiModelProperty(value = "设备IP地址")
	private String ipAddr;

	/**
	 * 设备平台来源
	 */
	@ApiModelProperty(value = "设备平台来源")
	private String source;

	/**
	 * 开关状态
	 */
	@ApiModelProperty(value = "开关状态")
	private Integer isEnable;
}
